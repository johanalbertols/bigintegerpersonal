/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class BigIntegerPersonal {
   
    private int miNumero[];
    public static BigIntegerPersonal ZERO = new BigIntegerPersonal("0");
    
    public BigIntegerPersonal() {
    }

    public BigIntegerPersonal(String miNumero){
          
                String arr[] = miNumero.split("");
                this.miNumero = new int[arr.length];
                for (int i = 0; i < arr.length; i++) {
                    this.miNumero[i] = Integer.parseInt(arr[i]);
                }
    }

    public int[] getMiNumero() {
        return miNumero;
    }
    /**
     * Mutiplica dos enteros BigInteger
     * @param dos
     * @return 
     */
    
    public BigIntegerPersonal multiply(BigIntegerPersonal dos)
    {
            int lleva = 0;
            int resultado[] = new int[this.miNumero.length + dos.miNumero.length + 1];
            int corre = resultado.length-1;
            int controlRes = corre;

            for (int i = dos.miNumero.length-1; i >=0; i--) {
                for (int j = this.miNumero.length-1; j >= 0; j--) 
                {
                    int remul = ((this.miNumero[j] * dos.miNumero[i]) + lleva + resultado[controlRes]);
                    resultado[controlRes] = remul%10;
                    lleva = remul / 10;
                    controlRes--;
                }
                resultado[controlRes] = lleva;
                lleva = 0;
                corre--;
                controlRes = corre;
            }
            this.miNumero = resultado;
            return this;
    }    
    
    /**
     * Retorna la representación entera del BigInteger_UFPS
     * @return un entero
     */
    public int intValue()
    {
        String value = "";
        for (int i = 0; i < this.miNumero.length; i++) {
            value += miNumero[i];
        }
        return Integer.parseInt(value);
    }

    @Override
    public String toString() {
        String rta = "";
        boolean contCeros = false;
        for (int i = 0; i < this.miNumero.length; i++) {
            if(this.miNumero[i] != 0){
                contCeros = true;
            }
            if(contCeros)
                rta += this.miNumero[i];
                
        }
        return rta;
    }
    
    
    
    
    
    
    
    
}
