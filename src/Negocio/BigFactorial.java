/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.BigIntegerPersonal;
import java.math.BigInteger;

/**
 *
 * @author madar
 */
public class BigFactorial {

    private BigInteger numero;
    
    public BigFactorial() {
    }
    

    
    public BigFactorial(String numero) throws Exception{
        
        validar(numero);
        BigInteger n=new BigInteger(numero);
        this.numero = n;
    }
    
    private void validar(String numero)throws Exception{
        for (int i = 0; i < numero.length(); i++) {
            if(!esNumero(numero.charAt(i)))
                throw new Exception("El numero no es valido");
        }
        BigInteger n=new BigInteger(numero);
        if (n.toString().equals(BigInteger.ZERO.toString())) 
            throw new Exception("No se puede calcular el factorial de 0 o negativo");
    }
    
    private boolean esNumero(char a){
        return a >= 49 && a <= 57 || a == '0';
    }
    
    /**
     * Obtiene el número factorial del atributo numero
     * @return un entero con el número factorial
     */
    public BigInteger getFactorial() 
    {   
        BigInteger fac=new BigInteger("1");
        for(int i=1;i<=this.numero.intValue();i++)
        {
            BigInteger i_big=new BigInteger(i+""); //23456
            fac=fac.multiply(i_big);
        }
        return fac;
    }

    @Override
    public String toString() {
        return this.numero.toString();
    }
    
    
    
    
    
    
    
    
}
